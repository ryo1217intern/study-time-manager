import React from 'react'
import { SubmitTime } from '@/components/submittime'

const TimeRegistration = () => {
  return (
    <div className='flex lg:flex-row flex-col lg:h-[550px] h-[1100px] border'>
        <div className='flex flex-col'>
                <SubmitTime />
                this week record
        </div>
        <div className='h-[550px] lg:w-1/2 w-full p-2 border'>
            this week graph
        </div>
    </div>
  )
}

export default TimeRegistration