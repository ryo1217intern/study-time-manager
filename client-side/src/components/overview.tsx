"use client"

import { Bar, BarChart, ResponsiveContainer, XAxis, YAxis } from "recharts"

const data = [
  {
    name: "Monday",
    total: 300,
  },
  {
    name: "Tuesday",
    total: 200,
  },
  {
    name: "Wednesday",
    total: 100,
  },
  {
    name: "Thursday",
    total: 400,
  },
  {
    name: "Friday",
    total: 500,
  },
  {
    name: "Saturday",
    total: 200,
  },
  {
    name: "Sunday",
    total: 700,
  },
]

export default function Overview() {
  return (
    <ResponsiveContainer width="100%" height={500}>
      <BarChart data={data}>
        <XAxis
          dataKey="name"
          stroke="#888888"
          fontSize={12}
          tickLine={false}
          axisLine={false}
        />
        <YAxis
          stroke="#888888"
          fontSize={12}
          tickLine={false}
          axisLine={false}
          tickFormatter={(value) => `${value}min`}
        />
        <Bar
          dataKey="total"
          fill="currentColor"
          radius={[6, 6, 0, 0]}
          className="fill-primary"
        />
      </BarChart>
    </ResponsiveContainer>
  )
}