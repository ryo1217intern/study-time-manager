export type mainTitle = {
    id: number
    main_title: string
    user_id: string
}

export type subTitle = {
    id: number
    sub_title: string
    user_id: string
}

export type studyTime = {
    id: number
    study_time: number
    study_date: Date
    study_main_title_id: number
    study_sub_title_id: number
    user_id: string
    created_at: Date
    updated_at: Date
}

export type CsrfToken = {
    csrf_token: string
}

export type userId = {
    user_id: string
}