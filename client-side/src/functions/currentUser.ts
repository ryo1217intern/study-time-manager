import { useUser } from "@clerk/nextjs";
import { cache } from "react";

export const currentUser = cache(async () => {
    const { user } = await useUser();
    return user;
});