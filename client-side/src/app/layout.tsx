"use client"

import "../app/globals.css"
import React, { ReactNode } from 'react';
import { ThemeProvider } from "@/app/themeProvider";
import { ClerkProvider } from "@clerk/nextjs"
import Background1 from "/public/topPageBackground1.svg"
import { SignIn, SignOutButton, SignInButton } from "@clerk/nextjs"
import { useUser } from "@clerk/nextjs";
import { useEffect } from "react";
interface RootLayoutProps {
  children: ReactNode;
}
import { Inter as FontSans } from "next/font/google"
import { cn } from "@/lib/utils"
import Link from "next/link";

const fontSans = FontSans({
  subsets: ["latin"],
  variable: "--font-sans",
})

export default function RootLayout({ children }: RootLayoutProps) {
  return (
    <ClerkProvider>
    <html lang="en" suppressHydrationWarning>
      <head />
      <body
        className={cn(
          "min-h-screen bg-background font-sans antialiased",
          fontSans.variable
        )}
      >
      <ThemeProvider
            attribute="class"
            defaultTheme="system"
            enableSystem
            disableTransitionOnChange
      >
      <header className="flex flex-col">
        <div className="h-16 flex items-center px-6 bg-[#5195E1] text-l text-white font-semibold pt-3">
          <div className="flex items-center space-x-5">
            <Link href="/">Home</Link>
            <Link href="/dashboard">Dashboard</Link>
            <Link href="/dashboard/timeregistration">Time</Link>
            <Link href="/dashboard/overview">Overview</Link>
            <Link href="/dashboard/analytics">Analytics</Link>
          </div>
          <div className="ml-auto flex items-center gap-3 space-x-5">
            <SignInButton />
            <SignOutButton />
          </div>
        </div>
        <div>
          <Background1 className="h-[80px] w-full"/>
        </div> 
      </header>
        {children}
      </ThemeProvider>
      </body>
    </html>
    </ClerkProvider>
  )
}