"use client"
import { useUser, SignIn, SignOutButton, SignInButton } from "@clerk/nextjs"
import DashboardIcon from "/public/dashboardIcon.svg"
import RightArrowIcon from "/public/rightArrowIcon.svg"
import Link from "next/link"
import axios from "axios"
import { useEffect } from "react"
import { CsrfToken } from "@/types"

export default function Home() {
	useEffect(() => {
		axios.defaults.withCredentials = true
		const getCsrfToken = async () => {
		  const { data } = await axios.get<CsrfToken>(
			`${process.env.NEXT_PUBLIC_REACT_APP_API_URL}/csrf`
		  )
		  axios.defaults.headers.common['X-CSRF-Token'] = data.csrf_token
		}
		getCsrfToken()
	  }, [])
	
  	const { user } = useUser();  // isLoadedを追加する

    return (
		<div className="w-screen h-screen flex flex-col">
			<div className="flex h-[70%] w-full items-center flex-col">
				<div className="flex items-center justify-center">
					<div className="w-full flex flex-col items-start justify-end p-20">
						<p className="text-4xl font-bold pb-4">
							Study Time Management Application
						</p>
						<p className="font-semibold">
							{user ? "Hello, Mr. "+ user.fullName +"!": "Hello John! Please Sign In!"}
							<br />
							Welcome to the Study Time Management Application!
							<br />
							In this application, users can record their study time.
							<br />
							It also calculates weekly statistics.
							<br />
							The operation is simple: just enter main title, sub title and time you studied.
							<br />
							I apologize, and I have a request...
							<br />
							When using this application, please use it on a PC with a large screen...
							<br />
							Due to my technical shorcomings, using it on smartphones or other devices may cause some problems...
						</p>
						<Link href="/dashboard" className="font-semibold pt-4 text-muted-foreground flex flex-row">
							<RightArrowIcon className="mr-2"/> <DashboardIcon className="mr-2"/> Let's go dashboard
						</Link>
					</div>
				</div>
			</div>		
		</div>
    );
}

