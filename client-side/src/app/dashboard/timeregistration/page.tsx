import React from 'react'
import TimeRegistration from '@/components/timeregistration'
export default function page() {
  return (
    <div>
        <h2>Time Registration</h2>
        <TimeRegistration />
    </div>
  )
}
