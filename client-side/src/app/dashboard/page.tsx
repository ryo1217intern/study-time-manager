"use client"
import React from 'react'
import { Tabs, TabsContent, TabsList, TabsTrigger } from "@/components/ui/tabs"
import Overview from "@/components/overview"
import TimeRegistration from "@/components/timeregistration"

const page = () => {
	return (
		<div className="flex flex-col h-screen w-screen">
      		<div className='h-4/5 flex flex-col'>
				<div className='w-auto'>
					<h1 className='text-4xl font-bold p-4'>Dashboard</h1>
				</div>
				<div className='h-[90%] p-4 w-full'>
					<Tabs defaultValue="account" className="w-full">
						<TabsList>
							<TabsTrigger value="time registration">Time Registration</TabsTrigger>
							<TabsTrigger value="analytics">Analytics</TabsTrigger>
							<TabsTrigger value="overview">Overview</TabsTrigger>
						</TabsList>
						<TabsContent value="time registration" className='w-full p-2'>
							<TimeRegistration />
						</TabsContent>
						<TabsContent value="analytics">
							No Content
						</TabsContent>
						<TabsContent value="overview">
							<Overview />
						</TabsContent>
					</Tabs>
				</div>
      		</div>
    	</div>
  )
}

export default page