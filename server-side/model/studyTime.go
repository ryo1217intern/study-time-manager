package model

import (
	"time"
)

type StudyTime struct {
	Id               int           `json:"id" gorm:"primary_key"`
	StudyTime        time.Duration `json:"study_time" gorm:"not null"`
	StudyDate        time.Time     `json:"study_date" gorm:"not null"`
	StudyMainTitleId  int           `json:"study_main_title_id"`
	StudySubTitleId   int           `json:"study_sub_title_id"`
	UserId           string        `json:"user_id" gorm:"not null"`
	CreatedAt        time.Time     `json:"created_at"`
	UpdatedAt        time.Time     `json:"updated_at"`

	// GORMのリレーションシップ定義
	StudyMainType    StudyMainTitle `gorm:"foreignkey:StudyMainTitleId"`
	StudySubType     StudySubTitle  `gorm:"foreignkey:StudySubTitleId"`
}

type TimeResponse struct {
	Id               int           `json:"id" gorm:"primary_key"`
	StudyTime        time.Duration `json:"study_time"`
	StudyDate        time.Time     `json:"study_date"`
	StudyMainTitleId  int           `json:"study_main_title_id"`
	StudySubTitleId   int           `json:"study_sub_title_id"`
}