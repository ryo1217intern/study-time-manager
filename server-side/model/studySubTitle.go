package model

type StudySubTitle struct {
	Id		int    `json:"id" gorm:"primary_key"`
    SubTitle   string `json:"sub_title" gorm:"not null"`
    UserId    string `json:"user_id" gorm:"not null"`

    // Relation to StudyTime
    StudyTimes []StudyTime `gorm:"foreignkey:StudySubTitleId"`
}

type SubTitleResponse struct {
    Id       int    `json:"id" gorm:"primary_key" `
    SubTitle string `json:"sub_title"`
}