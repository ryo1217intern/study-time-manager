package model

type StudyMainTitle struct {
	Id        int    `json:"id" gorm:"primary_key"`
    MainTitle  string `json:"main_title" gorm:"not null"`
    UserId    string `json:"user_id" gorm:"not null"`

    // Relation to StudyTime
    StudyTimes []StudyTime `gorm:"foreignkey:StudyMainTitleId"`
}

type MainTitleResponse struct {
    Id       int    `json:"id" gorm:"primary_key" `
    MainTitle string `json:"study_main_title"`
}