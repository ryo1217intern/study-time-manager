package controller

import (
	"fmt"
	"net/http"
	"server-side/model"
	"server-side/usecase"
	"strconv"

	"github.com/golang-jwt/jwt/v4"
	"github.com/labstack/echo/v4"
)

type IMainTitleController interface {
	GetAllMainTitle(c echo.Context) error
	CreateMainTitle(c echo.Context) error
	UpdateMainTitle(c echo.Context) error
	DeleteMainTitle(c echo.Context) error
}

type mainTitleController struct {
	mu usecase.IMainTitleUsecase
}

func NewMainTitleController(mu usecase.IMainTitleUsecase) IMainTitleController {
	return &mainTitleController{mu}
}

func (mc *mainTitleController) GetAllMainTitle(c echo.Context) error {
	// インターフェースを文字列に変換する際に、nil チェックを行う
	var userId string
	user := c.Get("user")
	if user != nil {
		userId, _ = user.(*jwt.Token).Claims.(jwt.MapClaims)["user_id"].(string)
	} else {
		// user が nil の場合に適切なエラー処理を行う
		// 例: デフォルトの値を設定する、エラーレスポンスを返すなど
		userId = "fail_user"
	}
	fmt.Println("User ID:", userId)
	mainTitlesRes, err := mc.mu.GetAllMainTitle(userId)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err.Error())
	}
	return c.JSON(http.StatusOK, mainTitlesRes)
}

func (mc *mainTitleController) CreateMainTitle(c echo.Context) error {
	// インターフェースを文字列に変換する際に、nil チェックを行う
	var userId string
	user := c.Get("user")
	if user != nil {
		userId, _ = user.(*jwt.Token).Claims.(jwt.MapClaims)["user_id"].(string)
	} else {
		// user が nil の場合に適切なエラー処理を行う
		// 例: デフォルトの値を設定する、エラーレスポンスを返すなど
		userId = "fail_user"
	}
	fmt.Println("User ID:", userId)
	mainTitle := model.StudyMainTitle{}
	if err := c.Bind(&mainTitle); err != nil {
		return c.JSON(http.StatusBadRequest, err.Error())
	}
	mainTitle.UserId = userId
	mainTitleRes, err := mc.mu.CreateMainTitle(&mainTitle)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err.Error())
	}
	return c.JSON(http.StatusCreated, mainTitleRes)
}

func (mc *mainTitleController) UpdateMainTitle(c echo.Context) error {
	// インターフェースを文字列に変換する際に、nil チェックを行う
	var userId string
	user := c.Get("user")
	if user != nil {
		userId, _ = user.(*jwt.Token).Claims.(jwt.MapClaims)["user_id"].(string)
	} else {
		// user が nil の場合に適切なエラー処理を行う
		// 例: デフォルトの値を設定する、エラーレスポンスを返すなど
		userId = "fail_user"
	}
	fmt.Println("User ID:", userId)
	id := c.Param("mainTitleId")
	newMainTitleId, _ := strconv.Atoi(id)
	fmt.Println("MainTitle ID:", newMainTitleId)
	newMainTitle := model.StudyMainTitle{}
	if err := c.Bind(&newMainTitle); err != nil {
		return c.JSON(http.StatusBadRequest, err.Error())
	}
	newMainTitle.Id = newMainTitleId

	mainTitleRes, err := mc.mu.UpdateMainTitle(userId, newMainTitle.Id, newMainTitle.MainTitle)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err.Error())
	}
	return c.JSON(http.StatusOK, mainTitleRes)
}

func (mc *mainTitleController) DeleteMainTitle(c echo.Context) error {
	// インターフェースを文字列に変換する際に、nil チェックを行う
	var userId string
	user := c.Get("user")
	if user != nil {
		userId, _ = user.(*jwt.Token).Claims.(jwt.MapClaims)["user_id"].(string)
	} else {
		// user が nil の場合に適切なエラー処理を行う
		// 例: デフォルトの値を設定する、エラーレスポンスを返すなど
		userId = "fail_user"
	}
	fmt.Println("User ID:", userId)
	id := c.Param("mainTitleId")
	mainTitleId, _ := strconv.Atoi(id)
	if err := mc.mu.DeleteMainTitle(userId, mainTitleId); err != nil {
		return c.JSON(http.StatusInternalServerError, err.Error())
	}
	return c.JSON(http.StatusOK, "MainTitle is deleted")
}



