package controller

import (
	"fmt"
	"net/http"
	"server-side/model"
	"server-side/usecase"
	"strconv"

	"github.com/golang-jwt/jwt/v4"
	"github.com/labstack/echo/v4"
)

type ITimeController interface {
	GetAllTime(c echo.Context) error
	CreateTime(c echo.Context) error
	UpdateTime(c echo.Context) error
	DeleteTime(c echo.Context) error
}

type timeController struct {
	tu usecase.ITimeUsecase
}

func NewTimeController(tu usecase.ITimeUsecase) ITimeController {
	return &timeController{tu}
}

func (tc *timeController) GetAllTime(c echo.Context) error {
	// インターフェースを文字列に変換する際に、nil チェックを行う
	var userId string
	user := c.Get("user")
	if user != nil {
		userId, _ = user.(*jwt.Token).Claims.(jwt.MapClaims)["user_id"].(string)
	} else {
		// user が nil の場合に適切なエラー処理を行う
		// 例: デフォルトの値を設定する、エラーレスポンスを返すなど
		userId = "fail_user"
	}
	fmt.Println("User ID:", userId)
	timesRes, err := tc.tu.GetAllTime(userId)
	if err != nil {
		return c.JSON(http.StatusBadRequest, err.Error())
	}
	return c.JSON(http.StatusOK, timesRes)
}

func (tc *timeController) CreateTime(c echo.Context) error {
	// インターフェースを文字列に変換する際に、nil チェックを行う
	var userId string
	user := c.Get("user")
	if user != nil {
		userId, _ = user.(*jwt.Token).Claims.(jwt.MapClaims)["user_id"].(string)
	} else {
		// user が nil の場合に適切なエラー処理を行う
		// 例: デフォルトの値を設定する、エラーレスポンスを返すなど
		userId = "fail_user"
	}
	fmt.Println("User ID:", userId)
	time := model.StudyTime{}
	if err := c.Bind(&time); err != nil {
		return c.JSON(http.StatusBadRequest, err.Error())
	}
	time.UserId = userId
	timeRes, err := tc.tu.CreateTime(&time)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err.Error())
	}
	return c.JSON(http.StatusCreated, timeRes)
}

func (tc *timeController) UpdateTime(c echo.Context) error {
	// インターフェースを文字列に変換する際に、nil チェックを行う
	var userId string
	user := c.Get("user")
	if user != nil {
		userId, _ = user.(*jwt.Token).Claims.(jwt.MapClaims)["user_id"].(string)
	} else {
		// user が nil の場合に適切なエラー処理を行う
		// 例: デフォルトの値を設定する、エラーレスポンスを返すなど
		userId = "fail_user"
	}
	fmt.Println("User ID:", userId)
	timeId, _ := strconv.Atoi(c.Param("timeId"))
	newTime := model.StudyTime{}
	if err := c.Bind(&newTime); err != nil {
		return c.JSON(http.StatusBadRequest, err.Error())
	}
	newTime.Id = timeId
	timeRes, err := tc.tu.UpdateTime(userId, newTime.Id, newTime)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err.Error())
	}
	return c.JSON(http.StatusOK, timeRes)
}

func (tc *timeController) DeleteTime(c echo.Context) error {
	// インターフェースを文字列に変換する際に、nil チェックを行う
	var userId string
	user := c.Get("user")
	if user != nil {
		userId, _ = user.(*jwt.Token).Claims.(jwt.MapClaims)["user_id"].(string)
	} else {
		// user が nil の場合に適切なエラー処理を行う
		// 例: デフォルトの値を設定する、エラーレスポンスを返すなど
		userId = "fail_user"
	}
	fmt.Println("User ID:", userId)
	timeId, _ := strconv.Atoi(c.Param("timeId"))
	err := tc.tu.DeleteTime(userId, timeId)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err.Error())
	}
	return c.JSON(http.StatusOK, "Time Deleted")
}
