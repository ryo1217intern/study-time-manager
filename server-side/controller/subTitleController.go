package controller

import (
	"net/http"
	"server-side/model"
	"server-side/usecase"
	"strconv"

	"github.com/golang-jwt/jwt/v4"
	"github.com/labstack/echo/v4"
)

type ISubTitleController interface {
	GetAllSubTitle(c echo.Context) error
	CreateSubTitle(c echo.Context) error
	UpdateSubTitle(c echo.Context) error
	DeleteSubTitle(c echo.Context) error
}

type subTitleController struct {
	su usecase.ISubTitleUsecase
}

func NewSubTitleController(su usecase.ISubTitleUsecase) ISubTitleController {
	return &subTitleController{su}
}

func (sc *subTitleController) GetAllSubTitle(c echo.Context) error {
	// インターフェースを文字列に変換する際に、nil チェックを行う
	var userId string
	user := c.Get("user")
	if user != nil {
		userId, _ = user.(*jwt.Token).Claims.(jwt.MapClaims)["user_id"].(string)
	} else {
		// user が nil の場合に適切なエラー処理を行う
		// 例: デフォルトの値を設定する、エラーレスポンスを返すなど
		userId = "fail_user"
	}
	subTitlesRes, err := sc.su.GetAllSubTitle(userId)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err.Error())
	}
	return c.JSON(http.StatusOK, subTitlesRes)
}
func (sc *subTitleController) CreateSubTitle(c echo.Context) error {
	// インターフェースを文字列に変換する際に、nil チェックを行う
	var userId string
	user := c.Get("user")
	if user != nil {
		userId, _ = user.(*jwt.Token).Claims.(jwt.MapClaims)["user_id"].(string)
	} else {
		// user が nil の場合に適切なエラー処理を行う
		// 例: デフォルトの値を設定する、エラーレスポンスを返すなど
		userId = "fail_user"
	}
	subTitle := model.StudySubTitle{}
	if err := c.Bind(&subTitle); err != nil {
		return c.JSON(http.StatusBadRequest, err.Error())
	}
	subTitle.UserId = userId
	subTitleRes, err := sc.su.CreateSubTitle(&subTitle)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err.Error())
	}
	return c.JSON(http.StatusCreated, subTitleRes)
}

func (sc *subTitleController) UpdateSubTitle(c echo.Context) error {
	// インターフェースを文字列に変換する際に、nil チェックを行う
	var userId string
	user := c.Get("user")
	if user != nil {
		userId, _ = user.(*jwt.Token).Claims.(jwt.MapClaims)["user_id"].(string)
	} else {
		// user が nil の場合に適切なエラー処理を行う
		// 例: デフォルトの値を設定する、エラーレスポンスを返すなど
		userId = "fail_user"
	}
	newSubTitleId, _ := strconv.Atoi(c.Param("subTitleId"))
	newSubTitle := model.StudySubTitle{}
	if err := c.Bind(&newSubTitle); err != nil {
		return c.JSON(http.StatusBadRequest, err.Error())
	}
	newSubTitle.Id = newSubTitleId
	subTitleRes, err := sc.su.UpdateSubTitle(userId, newSubTitle.Id, newSubTitle.SubTitle)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err.Error())
	}
	return c.JSON(http.StatusOK, subTitleRes)
}

func (sc *subTitleController) DeleteSubTitle(c echo.Context) error {
	// インターフェースを文字列に変換する際に、nil チェックを行う
	var userId string
	user := c.Get("user")
	if user != nil {
		userId, _ = user.(*jwt.Token).Claims.(jwt.MapClaims)["user_id"].(string)
	} else {
		// user が nil の場合に適切なエラー処理を行う
		// 例: デフォルトの値を設定する、エラーレスポンスを返すなど
		userId = "fail_user"
	}
	id, _ := strconv.Atoi(c.Param("subTitleId"))
	if err := sc.su.DeleteSubTitle(userId, id); err != nil {
		return c.JSON(http.StatusInternalServerError, err.Error())
	}
	return c.JSON(http.StatusOK, "Success delete")
}
