package router

import (
	"net/http"
	"os"
	"server-side/controller"

	echojwt "github.com/labstack/echo-jwt/v4"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func NewRouter(uc controller.IUserController, mc controller.IMainTitleController, sc controller.ISubTitleController, tc controller.ITimeController) *echo.Echo {
	e := echo.New()

	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"http://localhost:3000", os.Getenv("FE_URL")},
		AllowMethods: []string{"GET", "POST", "PUT", "DELETE"},
		AllowHeaders: []string{echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderAccept, echo.HeaderAccessControlAllowHeaders, echo.HeaderXCSRFToken},
		AllowCredentials: true,
	}))

	e.Use(middleware.CSRFWithConfig(middleware.CSRFConfig{
		CookiePath: "/",
		CookieDomain: os.Getenv("API_DOMAIN"),
		CookieHTTPOnly: true,
		CookieSameSite: http.SameSiteNoneMode,
		// CookieSameSite: http.SameSiteLaxMode,
		CookieMaxAge: 20000,
	}))

	e.POST("/login", uc.Login)
	e.POST("/logout", uc.Logout)

	// MainTitle
	m := e.Group("/main-title")
	m.Use(echojwt.WithConfig(echojwt.Config{
		SigningKey:  []byte(os.Getenv("SECRET")),
		TokenLookup: "cookie:token",
		ContextKey: "user",
	}))
	m.GET("", mc.GetAllMainTitle)
	m.POST("", mc.CreateMainTitle)
	m.PUT("/:mainTitleId", mc.UpdateMainTitle)
	m.DELETE("/:mainTitleId", mc.DeleteMainTitle)

	// SubTitle
	s := e.Group("/sub-title")
	s.Use(echojwt.WithConfig(echojwt.Config{
		SigningKey:  []byte(os.Getenv("SECRET")),
		TokenLookup: "cookie:token",
	}))
	s.GET("", sc.GetAllSubTitle)
	s.POST("", sc.CreateSubTitle)
	s.PUT("/:subTitleId", sc.UpdateSubTitle)
	s.DELETE("/:subTitleId", sc.DeleteSubTitle)

	// Time
	t := e.Group("/time")
	t.Use(echojwt.WithConfig(echojwt.Config{
		SigningKey:  []byte(os.Getenv("SECRET")),
		TokenLookup: "cookie:token",
	}))
	t.GET("", tc.GetAllTime)
	t.POST("", tc.CreateTime)
	t.PUT("/:timeId", tc.UpdateTime)
	t.DELETE("/:timeId", tc.DeleteTime)

	return e
}