package usecase

import (
	"server-side/model"
	"server-side/repository"
)

type ITimeUsecase interface {
	GetAllTime(userId string) ([]model.TimeResponse, error)
	CreateTime(time *model.StudyTime) (model.TimeResponse, error)
	UpdateTime(userId string, id int, updateTime model.StudyTime) (model.TimeResponse, error)
	DeleteTime(userId string, timeId int) error
}

type timeUsecase struct {
	tr repository.IStudyTimeRepository
}

func NewTimeUsecase(tr repository.IStudyTimeRepository) ITimeUsecase {
	return &timeUsecase{tr}
}

func (tu *timeUsecase) GetAllTime(userId string) ([]model.TimeResponse, error) {
	times := []model.StudyTime{}
	if err := tu.tr.GetAllStudyTime(&times, userId); err != nil {
		return nil, err
	}
	resTimes := []model.TimeResponse{}
	for _, time := range times {
		t := model.TimeResponse{
			Id:        time.Id,
			StudyTime: time.StudyTime,
			StudyDate: time.StudyDate,
			StudyMainTitleId: time.StudyMainTitleId,
			StudySubTitleId: time.StudySubTitleId,
		}
		resTimes = append(resTimes, t)
	}
	return resTimes, nil
}

func (tu *timeUsecase) CreateTime(time *model.StudyTime) (model.TimeResponse, error) {
	if err := tu.tr.CreateStudyTime(time); err != nil {
		return model.TimeResponse{}, err
	}
	resTime := model.TimeResponse{
		Id:        time.Id,
		StudyTime: time.StudyTime,
		StudyDate: time.StudyDate,
		StudyMainTitleId: time.StudyMainTitleId,
		StudySubTitleId: time.StudySubTitleId,
	}
	return resTime, nil
}

func (tu *timeUsecase) UpdateTime(userId string, id int, updateTime model.StudyTime) (model.TimeResponse, error) {
	if err := tu.tr.UpdateStudyTime(userId, id, updateTime); err != nil {
		return model.TimeResponse{}, err
	}
	resTime := model.TimeResponse{
		Id:        id,
		StudyTime: updateTime.StudyTime,
		StudyDate: updateTime.StudyDate,
		StudyMainTitleId: updateTime.StudyMainTitleId,
		StudySubTitleId: updateTime.StudySubTitleId,
	}
	return resTime, nil
}

func (tu *timeUsecase) DeleteTime(userId string, timeId int) error {
	if err := tu.tr.DeleteStudyTime(userId, timeId); err != nil {
		return err
	}
	return nil
}

