package usecase

import (
	"server-side/model"
	"server-side/repository"
)

type ISubTitleUsecase interface {
	GetAllSubTitle(userId string) ([]model.SubTitleResponse, error)
	CreateSubTitle(SubTitle *model.StudySubTitle) (model.SubTitleResponse, error)
	UpdateSubTitle(userId string, id int, newSubTitle string) (model.SubTitleResponse, error)
	DeleteSubTitle(userId string, SubTitleId int) error
}

type SubTitleUsecase struct {
	sr repository.IStudySubTitleRepository
}

func NewSubTitleUsecase(sr repository.IStudySubTitleRepository) ISubTitleUsecase {
	return &SubTitleUsecase{sr}
}

func (mu *SubTitleUsecase) GetAllSubTitle(userId string) ([]model.SubTitleResponse, error) {
	SubTitles := []model.StudySubTitle{}
	if err := mu.sr.GetAllSubTitle(&SubTitles, userId); err != nil {
		return nil, err
	}
	resSubTitles := []model.SubTitleResponse{}
	for _, SubTitle := range SubTitles {
		t := model.SubTitleResponse{
			Id:        SubTitle.Id,
			SubTitle: SubTitle.SubTitle,
		}
		resSubTitles = append(resSubTitles, t)
	}
	return resSubTitles, nil
}

func (mu *SubTitleUsecase) CreateSubTitle(SubTitle *model.StudySubTitle) (model.SubTitleResponse, error) {
	if err := mu.sr.CreateSubTitle(SubTitle); err != nil {
		return model.SubTitleResponse{}, err
	}
	resSubTitle := model.SubTitleResponse{
		Id:        SubTitle.Id,
		SubTitle: SubTitle.SubTitle,
	}
	return resSubTitle, nil
}

func (mu *SubTitleUsecase) UpdateSubTitle(userId string, id int, newSubTitle string) (model.SubTitleResponse, error) {
	if err := mu.sr.UpdateSubTitle(userId, id, newSubTitle); err != nil {
		return model.SubTitleResponse{}, err
	}
	resSubTitle := model.SubTitleResponse{
		Id:        id,
		SubTitle: newSubTitle,
	}
	return resSubTitle, nil
}

func (mu *SubTitleUsecase) DeleteSubTitle(userId string, SubTitleId int) error {
	if err := mu.sr.DeleteSubTitle(userId, SubTitleId); err != nil {
		return err
	}
	return nil
}