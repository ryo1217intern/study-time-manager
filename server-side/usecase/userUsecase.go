package usecase

import (
	"os"
	"time"

	"github.com/golang-jwt/jwt/v5"
)

type IUserUsecase interface {
	Login(userId string) (string, error)
}

type UserUsecase struct {}

func NewUserUsecase() IUserUsecase {
	return &UserUsecase{}
}

func (uu *UserUsecase) Login(userId string) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"user_id": userId,
		"exp": time.Now().Add(time.Hour * 24).Unix(),
	})
	tokenString, err := token.SignedString([]byte(os.Getenv("SECRET")))
	if err != nil {
		return "", err
	}
	return tokenString, nil
}