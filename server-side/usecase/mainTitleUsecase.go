package usecase

import (
	"server-side/model"
	"server-side/repository"
)

type IMainTitleUsecase interface {
	GetAllMainTitle(userId string) ([]model.MainTitleResponse, error)
	CreateMainTitle(mainTitle *model.StudyMainTitle) (model.MainTitleResponse, error)
	UpdateMainTitle(userId string, id int, newMainTitle string) (model.MainTitleResponse, error)
	DeleteMainTitle(userId string, mainTitleId int) error
}

type mainTitleUsecase struct {
	mr repository.IStudyMainTitleRepository
}

func NewMainTitleUsecase(mr repository.IStudyMainTitleRepository) IMainTitleUsecase {
	return &mainTitleUsecase{mr}
}

func (mu *mainTitleUsecase) GetAllMainTitle(userId string) ([]model.MainTitleResponse, error) {
	mainTitles := []model.StudyMainTitle{}
	if err := mu.mr.GetAllMainTitle(&mainTitles, userId); err != nil {
		return nil, err
	}
	resMainTitles := []model.MainTitleResponse{}
	for _, mainTitle := range mainTitles {
		t := model.MainTitleResponse{
			Id:        mainTitle.Id,
			MainTitle: mainTitle.MainTitle,
		}
		resMainTitles = append(resMainTitles, t)
	}
	return resMainTitles, nil
}

func (mu *mainTitleUsecase) CreateMainTitle(mainTitle *model.StudyMainTitle) (model.MainTitleResponse, error) {
	if err := mu.mr.CreateMainTitle(mainTitle); err != nil {
		return model.MainTitleResponse{}, err
	}
	resMainTitle := model.MainTitleResponse{
		Id:        mainTitle.Id,
		MainTitle: mainTitle.MainTitle,
	}
	return resMainTitle, nil
}

func (mu *mainTitleUsecase) UpdateMainTitle(userId string, id int, newMainTitle string) (model.MainTitleResponse, error) {
	if err := mu.mr.UpdateMainTitle(userId, id, newMainTitle); err != nil {
		return model.MainTitleResponse{}, err
	}
	resMainTitle := model.MainTitleResponse{
		Id:        id,
		MainTitle: newMainTitle,
	}
	return resMainTitle, nil
}

func (mu *mainTitleUsecase) DeleteMainTitle(userId string, mainTitleId int) error {
	if err := mu.mr.DeleteMainTitle(userId, mainTitleId); err != nil {
		return err
	}
	return nil
}