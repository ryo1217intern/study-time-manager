package main

import (
	"fmt"
	"server-side/db"
	"server-side/model"
)

func main() {
	dbConn := db.NewDB()

	defer fmt.Println("Success migrate database")
	defer db.CloseDB(dbConn)

	dbConn.AutoMigrate(
		&model.StudyTime{},
		&model.StudyMainTitle{},
		&model.StudySubTitle{},
	)
}