package main

import (
	"server-side/controller"
	"server-side/db"
	"server-side/repository"
	"server-side/router"
	"server-side/usecase"
)

func main() {
	db := db.NewDB()
	
	mainTitleRepository := repository.NewStudyMainTitleRepository(db)
	subTitleRepository := repository.NewStudySubTitleRepository(db)
	studyTimeRepository := repository.NewStudyTimeRepository(db)

	userUsecase := usecase.NewUserUsecase()
	mainTitleUsecase := usecase.NewMainTitleUsecase(mainTitleRepository)
	subTitleUsecase := usecase.NewSubTitleUsecase(subTitleRepository)
	timeUsecase := usecase.NewTimeUsecase(studyTimeRepository)

	userController := controller.NewUserController(userUsecase)
	mainTitleController := controller.NewMainTitleController(mainTitleUsecase)
	subTitleController := controller.NewSubTitleController(subTitleUsecase)
	timeController := controller.NewTimeController(timeUsecase)

	e := router.NewRouter(userController, mainTitleController, subTitleController, timeController)
	e.Logger.Fatal(e.Start(":8080"))
}