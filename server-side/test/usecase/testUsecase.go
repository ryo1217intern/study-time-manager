package main

import (
	"fmt"
	"server-side/db"
	"server-side/model"
	"server-side/repository"
	"server-side/usecase"
	"time"
)


func main() {
	// testMainTitleUsecase()
	// testSubTitleUsecase()
	testTimeUsecase()
}

func testMainTitleUsecase() {
	fmt.Println("Start test MainTitleUsecase")
	defer fmt.Println("Success test MainTitleUsecase")

	db := db.NewDB()
	repo := repository.NewStudyMainTitleRepository(db)
	usecase := usecase.NewMainTitleUsecase(repo)
	userId := "test_user"
	var id int

	// Test CreateMainTitle
	createMainTitle := model.StudyMainTitle{
		MainTitle: "test main title",
		UserId:    userId,
	}
	
	if res, err := usecase.CreateMainTitle(&createMainTitle); err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(res)
	}

	// Test GetAllMainTitle
	if res, err := usecase.GetAllMainTitle(userId); err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(res)
		id = res[2].Id
	}


	// Test UpdateMainTitle
	newMainTitle := "updated"
	if res, err := usecase.UpdateMainTitle(userId, id, newMainTitle); err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(res)
	}

	// Test DeleteMainTitle
	if err := usecase.DeleteMainTitle(userId, id); err != nil {
		fmt.Println(err)
	} else {
		fmt.Println("Success delete")
	}

	// Test GetAllMainTitle
	if res, err := usecase.GetAllMainTitle(userId); err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(res)
	}
}

func testSubTitleUsecase() {
	fmt.Println("Start test SubTitleUsecase")
	defer fmt.Println("Success test SubTitleUsecase")
	db := db.NewDB()
	repo := repository.NewStudySubTitleRepository(db)
	usecase := usecase.NewSubTitleUsecase(repo)
	userId := "test_user"
	var id int

	// Test CreateSubTitle
	createSubTitle := model.StudySubTitle{
		SubTitle: "test sub title",
		UserId:   userId,
	}
	
	if res, err := usecase.CreateSubTitle(&createSubTitle); err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(res)
	}

	// Test GetAllSubTitle
	if res, err := usecase.GetAllSubTitle(userId); err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(res)
		id = res[2].Id
	}

	// Test UpdateSubTitle
	newSubTitle := "updated"
	if res, err := usecase.UpdateSubTitle(userId, id, newSubTitle); err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(res)
	}

	// Test DeleteSubTitle
	if err := usecase.DeleteSubTitle(userId, id); err != nil {
		fmt.Println(err)
	} else {
		fmt.Println("Success delete")
	}

	// Test GetAllSubTitle
	if res, err := usecase.GetAllSubTitle(userId); err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(res)
	}
}

func testTimeUsecase() {
	fmt.Println("Start test TimeUsecase")
	defer fmt.Println("Success test TimeUsecase")

	db := db.NewDB()
	repo := repository.NewStudyTimeRepository(db)
	usecase := usecase.NewTimeUsecase(repo)
	userId := "test_user"
	var id int

	// Test CreateTime
	createTime := model.StudyTime{
		StudyTime:        100,
		StudyDate: 	  time.Now(),
		StudyMainTitleId: 30,
		StudySubTitleId:  1,
		UserId:           userId,
	}
	
	if res, err := usecase.CreateTime(&createTime); err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(res)
	}

	// Test GetAllTime
	if res, err := usecase.GetAllTime(userId); err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(res)
		id = res[0].Id
	}

	// Test UpdateTime
	newTime := model.StudyTime{
		StudyTime: 200,
		StudyMainTitleId: 31,
		StudySubTitleId: 2,
	}
	if res, err := usecase.UpdateTime(userId, id, newTime); err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(res)
	}

	// // Test DeleteTime
	// if err := usecase.DeleteTime(userId, id); err != nil {
	// 	fmt.Println(err)
	// } else {
	// 	fmt.Println("Success delete")
	// }

	// // Test GetAllTime
	// if res, err := usecase.GetAllTime(userId); err != nil {
	// 	fmt.Println(err)
	// } else {
	// 	fmt.Println(res)
	// }
}