package main

import (
	"fmt"
	"server-side/db"
	"server-side/model"
	"server-side/repository"
	"time"
)

func main() {
	defer fmt.Println("Success test repository")
	testMainTitle()
	testSubTitle()
	testTime()
}

func testMainTitle() {
	db := db.NewDB()
	repo := repository.NewStudyMainTitleRepository(db)
	userId := "test_user"

	// Test CreateMainTitle
	mainTitle := model.StudyMainTitle{
		MainTitle: "test main title",
		UserId:    userId,
	}
	repo.CreateMainTitle(&mainTitle)

	// Test GetAllMainTitle
	createMainTitles := []model.StudyMainTitle{}
	if err := repo.GetAllMainTitle(&createMainTitles, userId); err != nil {
		fmt.Println(err)
	}
	fmt.Println(createMainTitles)

	

	id := createMainTitles[2].Id
	// Test UpdateMainTitle
	if err := repo.UpdateMainTitle(userId, id, "updated"); err != nil {
		fmt.Println(err)
	}

	// Test GetAllMainTitle
	updateMainTitles := []model.StudyMainTitle{}
	if err := repo.GetAllMainTitle(&updateMainTitles, userId); err != nil {
		fmt.Println(err)
	}
	fmt.Println(updateMainTitles)



	// Test DeleteMainTitle
	if err := repo.DeleteMainTitle(userId, id); err != nil {
		fmt.Println(err)
	}

	// Test GetAllMainTitle
	deleteMainTitles := []model.StudyMainTitle{}
	if err := repo.GetAllMainTitle(&deleteMainTitles, userId); err != nil {
		fmt.Println(err)
	}
	fmt.Println(deleteMainTitles)
}


func testSubTitle() {
	db := db.NewDB()
	repo := repository.NewStudySubTitleRepository(db)
	userId := "test_user"

	// Test CreateSubTitle
	subTitle := model.StudySubTitle{
		SubTitle: "test sub title",
		UserId:   userId,
	}
	repo.CreateSubTitle(&subTitle)

	// Test GetAllSubTitle
	createSubTitles := []model.StudySubTitle{}
	if err := repo.GetAllSubTitle(&createSubTitles, userId); err != nil {
		fmt.Println(err)
	}
	fmt.Println(createSubTitles)

	id := createSubTitles[2].Id
	// Test UpdateSubTitle
	if err := repo.UpdateSubTitle(userId, id, "updated"); err != nil {
		fmt.Println(err)
	}

	// Test GetAllSubTitle
	updateSubTitles := []model.StudySubTitle{}
	if err := repo.GetAllSubTitle(&updateSubTitles, userId); err != nil {
		fmt.Println(err)
	}
	fmt.Println(updateSubTitles)

	// Test DeleteSubTitle
	if err := repo.DeleteSubTitle(userId, id); err != nil {
		fmt.Println(err)
	}

	// Test GetAllSubTitle
	deleteSubTitles := []model.StudySubTitle{}
	if err := repo.GetAllSubTitle(&deleteSubTitles, userId); err != nil {
		fmt.Println(err)
	}
	fmt.Println(deleteSubTitles)
}

func testTime() {
	db := db.NewDB()
	repo := repository.NewStudyTimeRepository(db)
	userId := "test_user"

	// Test CreateTime
	time := model.StudyTime{
		StudyTime: 100,
		StudyDate: time.Now(),
		StudyMainTitleId: 1,
		StudySubTitleId: 1,
		UserId: userId,
	}
	repo.CreateStudyTime(&time)

	// Test GetAllTime
	createTimes := []model.StudyTime{}
	if err := repo.GetAllStudyTime(&createTimes, userId); err != nil {
		fmt.Println(err)
	}
	fmt.Println(createTimes)

	id := createTimes[0].Id
	updatedTime := model.StudyTime{
		StudyTime: 200,
		StudyMainTitleId: 2,
		StudySubTitleId: 2,
	}
	// Test UpdateTime
	if err := repo.UpdateStudyTime(userId, id, updatedTime); err != nil {
		fmt.Println(err)
	}

	// Test GetAllTime
	updateTimes := []model.StudyTime{}
	if err := repo.GetAllStudyTime(&updateTimes, userId); err != nil {
		fmt.Println(err)
	}
	fmt.Println(updateTimes)

	// Test DeleteTime
	if err := repo.DeleteStudyTime(userId, id); err != nil {
		fmt.Println(err)
	}

	// Test GetAllTime
	deleteTimes := []model.StudyTime{}
	if err := repo.GetAllStudyTime(&deleteTimes, userId); err != nil {
		fmt.Println(err)
	}
	fmt.Println(deleteTimes)
}