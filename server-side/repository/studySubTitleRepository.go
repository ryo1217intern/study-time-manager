package repository

import (
	"server-side/model"

	"gorm.io/gorm"
)

type IStudySubTitleRepository interface {
	GetAllSubTitle(SubTitles *[]model.StudySubTitle, userId string) error
	CreateSubTitle(SubTitle *model.StudySubTitle) error
	UpdateSubTitle(userId string, id int, newSubTitle string) error
	DeleteSubTitle(userId string, studySubTitleId int) error
}

type studySubTitleRepository struct {
	db *gorm.DB
}

func NewStudySubTitleRepository(db *gorm.DB) IStudySubTitleRepository {
	return &studySubTitleRepository{db}
}

func (sr *studySubTitleRepository) GetAllSubTitle(SubTitles *[]model.StudySubTitle, userId string) error {
	if err := sr.db.Where("user_id = ?", userId).Find(SubTitles).Error; err != nil {
		return err
	}
	return nil
}

func (sr *studySubTitleRepository) CreateSubTitle(SubTitle *model.StudySubTitle) error {
	if err := sr.db.Create(SubTitle).Error; err != nil {
		return err
	}
	return nil
}

func (sr *studySubTitleRepository) UpdateSubTitle(userId string, id int, newSubTitle string) error {
    // Update operation
    result := sr.db.Model(&model.StudySubTitle{}).Where("user_id = ? AND id = ?", userId, id).Update("sub_title", newSubTitle)
    if result.Error != nil {
        return result.Error
    }
    return nil
}

func (sr *studySubTitleRepository) DeleteSubTitle(userId string, studySubTitleId int) error {
	if err := sr.db.Where("user_id = ? AND id = ?", userId, studySubTitleId).Delete(&model.StudySubTitle{}).Error; err != nil {
		return err
	}
	return nil
}