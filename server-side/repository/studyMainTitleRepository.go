package repository

import (
	"server-side/model"

	"gorm.io/gorm"
)

type IStudyMainTitleRepository interface {
	GetAllMainTitle(mainTitles *[]model.StudyMainTitle, userId string) error
	CreateMainTitle(mainTitle *model.StudyMainTitle) error
	UpdateMainTitle(userId string, id int, newMainTitle string) error
	DeleteMainTitle(userId string, studyMainTitleId int) error
}

type studyMainTitleRepository struct {
	db *gorm.DB
}

func NewStudyMainTitleRepository(db *gorm.DB) IStudyMainTitleRepository {
	return &studyMainTitleRepository{db}
}

func (mr *studyMainTitleRepository) GetAllMainTitle(mainTitles *[]model.StudyMainTitle, userId string) error {
	if err := mr.db.Where("user_id = ?", userId).Find(mainTitles).Error; err != nil {
		return err
	}
	return nil
}

func (mr *studyMainTitleRepository) CreateMainTitle(mainTitle *model.StudyMainTitle) error {
	if err := mr.db.Create(mainTitle).Error; err != nil {
		return err
	}
	return nil
}

func (mr *studyMainTitleRepository) UpdateMainTitle(userId string, id int, newMainTitle string) error {
    // Update operation
    result := mr.db.Model(&model.StudyMainTitle{}).Where("user_id = ? AND id = ?", userId, id).Update("main_title", newMainTitle)
    if result.Error != nil {
        return result.Error
    }
    return nil
}

func (mr *studyMainTitleRepository) DeleteMainTitle(userId string, studyMainTitleId int) error {
	if err := mr.db.Where("user_id = ? AND id = ?", userId, studyMainTitleId).Delete(&model.StudyMainTitle{}).Error; err != nil {
		return err
	}
	return nil
}
