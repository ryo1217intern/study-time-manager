package repository

import (
	"server-side/model"

	"gorm.io/gorm"
)

type IStudyTimeRepository interface {
	GetAllStudyTime(studyTimes *[]model.StudyTime, userId string) error
	GetStudyTimeById(studyTime *model.StudyTime, userId string, id int) error
	CreateStudyTime(studyTime *model.StudyTime) error
	UpdateStudyTime(userId string, id int, newStudyTime model.StudyTime) error
	DeleteStudyTime(userId string, studyTimeId int) error
}

type studyTimeRepository struct {
	db *gorm.DB
}

func NewStudyTimeRepository(db *gorm.DB) IStudyTimeRepository {
	return &studyTimeRepository{db}
}

func (tr *studyTimeRepository) GetAllStudyTime(studyTimes *[]model.StudyTime, userId string) error {
	if err := tr.db.Where("user_id = ?", userId).Order("created_at").Find(studyTimes).Error; err != nil {
		return err
	}
	return nil
}

func (tr *studyTimeRepository) GetStudyTimeById(studyTime *model.StudyTime, userId string, id int) error {
	if err := tr.db.Where("user_id = ? AND id = ?", userId, id).Find(studyTime).Error; err != nil {
		return err
	}
	return nil
}

func (tr *studyTimeRepository) CreateStudyTime(studyTime *model.StudyTime) error {
	if err := tr.db.Create(studyTime).Error; err != nil {
		return err
	}
	return nil
}

func (tr *studyTimeRepository) UpdateStudyTime(userId string, id int, newStudyTime model.StudyTime) error {
	// Update operation
	result := tr.db.Model(&model.StudyTime{}).Where("user_id = ? AND id = ?", userId, id).Updates(newStudyTime)
	if result.Error != nil {
		return result.Error
	}
	return nil
}

func (tr *studyTimeRepository) DeleteStudyTime(userId string, studyTimeId int) error {
	if err := tr.db.Where("user_id = ? AND id = ?", userId, studyTimeId).Delete(&model.StudyTime{}).Error; err != nil {
		return err
	}
	return nil
} 